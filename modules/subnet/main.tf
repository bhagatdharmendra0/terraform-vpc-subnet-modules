resource "aws_subnet" "public-subnet" {
  vpc_id     = var.vpc_id
  cidr_block = "10.0.30.0/24"
  availability_zone = "ap-south-1a"

  tags = {
    Name = "public-subnet"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = var.vpc_id
  tags = {
    Name = "terraform-igw"
  }
}

resource "aws_route_table" "r" {
  vpc_id = var.vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  tags = {
	"Name" = "public-subnet-route"
  }

}

resource "aws_route_table_association" "associate-public-subnet" {
  subnet_id      = aws_subnet.public-subnet.id
  route_table_id = aws_route_table.r.id
}
