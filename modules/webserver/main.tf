resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    description = "TLS from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["157.40.80.3/32"]
  }
      ingress {
    description = "TLS from VPC"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "terrraform-vpc-SG"
  }
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = file(var.key-pair)
  }
### ec2
resource "aws_instance" "nginx" {
  ami           = "ami-068d43a544160b7ef"
  instance_type = "t2.micro"
  associate_public_ip_address = true
  subnet_id = var.subnet-server
  vpc_security_group_ids = [ aws_security_group.allow_tls.id ]
  key_name = aws_key_pair.deployer.key_name
  tags = {
    Name = "terraform-nginx"
  }
}

resource "null_resource" "cluster" {
  depends_on = [aws_instance.nginx]
  
  connection {
    type = "ssh"
    user = "ec2-user"
    private_key = file("C:/Users/HP/.ssh/id_rsa")
    host = aws_instance.nginx.public_ip   
  }

  provisioner "remote-exec" {
    inline = ["sudo yum install docker -y" , 
    "sudo systemctl enable docker --now",
    "sudo docker run -itd --name sonuapp -p 8080:80 nginx  "
    ]
  }
  provisioner "file" {
    source = "test-send.txt"
    destination = "/home/ec2-user/test-copy.txt"
 }
  
provisioner "local-exec" {
   command =  "echo ${aws_instance.nginx.public_ip}  >> private_ips.txt"
     
  }

}