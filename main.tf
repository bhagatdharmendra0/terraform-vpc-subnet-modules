provider aws {
	region = "ap-south-1"
	access_key = var.access_key
  secret_key = var.secret_key
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  instance_tenancy = "default"
  tags = {
    Name = "terraform-vpc"
  }
}
module "public-subnet" {
  source = "./modules/subnet"
  vpc_id = aws_vpc.main.id
}


module "webserver" {
  source = "./modules/webserver"
  vpc_id = aws_vpc.main.id
  key-pair = "bhagat-laptopkey"
  subnet-server = module.public-subnet.subnet-output.id
  
  
}



output "ip_instance" {
  value = module.webserver.webserver1.public_ip
  
}
